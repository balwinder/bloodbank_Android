package com.example.balwindersingh.bloodhelp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.github.arturogutierrez.Badges;
import com.github.arturogutierrez.BadgesNotSupportedException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.HashMap;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class MainActivity extends AppCompatActivity {

    io.socket.client.Socket socket;
    Button signup,login;
    EditText email,password;
    String  name;
    ipaddress ipadress = new ipaddress();

    String emailforpass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        signup = findViewById(R.id.signup);
        login = findViewById(R.id.login);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        signup();
        login();
        Toast.makeText(getApplicationContext(),ipadress.ip,Toast.LENGTH_LONG).show();


    }




    public void signup(){

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(MainActivity.this,signup.class);
                startActivity(intent);


            }
        });


    }

    public void login(){

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                emailforpass =email.getText().toString();

                final RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

                String JsonURL = ipadress.ip+"/user/login";
                HashMap data = new HashMap();

                data.put("email",email.getText().toString());
                data.put("password",password.getText().toString());

                JsonObjectRequest objectRequest = new JsonObjectRequest(Request.Method.POST, JsonURL,new JSONObject(data),new Response.Listener<JSONObject>() {

                    public void onResponse(JSONObject response) {
                      //  Log.d("Hz", response.toString());
//                       try {
//                             token = response.getJSONObject("token");
//                      tokennumber = token.toString();
                        //tokenname = token.toString();
//                            //Toast.makeText(getApplicationContext(),token.toString(),Toast.LENGTH_LONG).show();
                        try {
                            name = response.get("token").toString();

                            sessionclass sessionclass = new sessionclass(MainActivity.this) ;
                            sessionclass.setToken(name);
                            sessionclass.setName(email.getText().toString());
                            socket = IO.socket(ipadress.ip);
                            Toast.makeText(getApplicationContext(), "connected",Toast.LENGTH_LONG).show();
                           // Connect(null);


                        }
                        catch (Exception e) {
                       e.printStackTrace();
                   }

//              }catch (JSONException e) {
//                        e.printStackTrace();
//                     }
                       // Toast.makeText(getApplicationContext(), name, Toast.LENGTH_LONG).show();
                        Intent intent= new Intent(MainActivity.this,showuserdetail.class);
                          intent.putExtra("token", name);
                        intent.putExtra("email", email.getText().toString());

                        startActivity(intent);

                        }
                }, new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
                );
                requestQueue.add(objectRequest);




             //       public void onResponse(JSONObject response) {
//                        Log.d("Hz",response.toString());
//                        try {
//                            JSONObject name = response.getJSONObject("info");
//                           // JSONArray name2 = response.getJSONArray("info");
//                            JSONArray name1 = response.getJSONArray("results");
//                            //Toast.makeText(getApplicationContext(),name2.getJSONArray(0).toString(),Toast.LENGTH_LONG).show();
//                           // Toast.makeText(getApplicationContext(),name.getString("seed"),Toast.LENGTH_LONG).show();
//                            //Toast.makeText(getApplicationContext(),name1.getJSONObject(0).getJSONObject("name").getString("first"),Toast.LENGTH_LONG).show();
//                            //Toast.makeText(getApplicationContext(),name.getString("page"),Toast.LENGTH_LONG).show();
//                           // Toast.makeText(getApplicationContext(),name1.getJSONObject(0).getString("gender"),Toast.LENGTH_LONG).show();
//                            Toast.makeText(getApplicationContext(),name1.getJSONObject(0).getJSONObject("name").getString("first"),Toast.LENGTH_LONG).show();
//                           // Toast.makeText(getApplicationContext(),name2.getString(0).toString(),Toast.LENGTH_LONG).show();
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }


               // Toast.makeText(getApplicationContext(),tokennumber,Toast.LENGTH_LONG).show();



            }
        });



    }




    public void Connect(View view) {
        Toast.makeText(getApplicationContext(), "Server Connected", Toast.LENGTH_LONG).show();
        socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {



            @Override
            public void call(Object... args) {

                runOnUiThread(new Runnable() {
                    public void run() {


                        Toast.makeText(getApplicationContext(), "Server Connected", Toast.LENGTH_LONG).show();
                    }
                });
                JSONObject obj = new JSONObject();
                try {
                    obj.put("User",email.getText().toString() );
                } catch (JSONException e) {
                    Log.d("Hz",e.getMessage());
                }
                socket.emit("UserConnected", obj);
            }

        });
        socket.connect();
    }

    public void tenntification(View view){
        try {
            Badges.setBadge(MainActivity.this, 5);
            Toast.makeText(getApplicationContext(),"notification is working",Toast.LENGTH_LONG).show();
        } catch (BadgesNotSupportedException badgesNotSupportedException) {
         Toast.makeText(getApplicationContext(),"erroe in notification",Toast.LENGTH_LONG).show();
        }
    }



}
