package com.example.balwindersingh.bloodhelp;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class bloodtypedetail extends AppCompatActivity {
    ArrayAdapter adapter;
    ListView listView;
    ArrayList<String> thelist;
    String email;

String User;

int count;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bloodtypedetail);


        listView = (ListView) findViewById(R.id.listview);

        thelist = new ArrayList<>();
        adapter = new ArrayAdapter<>(bloodtypedetail.this,android.R.layout.simple_list_item_1,thelist);
        listView.setAdapter(adapter);


        showdata();
    }


    public void showdata() {
//
//        final RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
//
//        String JsonURL = "http://192.168.2.18:3000/user/";
//        JsonObjectRequest objectRequest = new JsonObjectRequest(Request.Method.GET, JsonURL,new Response.Listener<JSONObject>() {
//
//            public void onResponse(JSONObject response) {
              //  Log.d("Hz",response.toString());
                try {

                    Intent intent = getIntent();
                    String result = intent.getStringExtra("count");

                     email = intent.getStringExtra("email");

                    User = intent.getStringExtra("User2");

                    try {
                        JSONArray jsonArr = new JSONArray(User);
                       // Toast.makeText(getApplicationContext(),jsonArr.toString(),Toast.LENGTH_LONG).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    count = Integer.parseInt(result);
for(int i=0;i<count;i++) {
    JSONArray jsonArr = new JSONArray(User);
    String list = jsonArr.getJSONObject(i).getString("email");
    thelist.add(list);
    //thelist1.add(data.getString(2));
    ListAdapter listAdapter = new ArrayAdapter<>(getApplication(), android.R.layout.simple_list_item_1, thelist);
    listView.setAdapter(listAdapter);
}



                } catch (JSONException e) {
                    e.printStackTrace();
                }


                // Toast.makeText(getApplicationContext(),response.toString(),Toast.LENGTH_LONG).show();

           // }
//        }, new Response.ErrorListener() {
//            public void onErrorResponse(VolleyError error) {
//                error.printStackTrace();
//            }
//        }
//        ) {
//
//            /**
//             * Passing some request headers*
//             */
//            final sessionclass sessionclass = new sessionclass(bloodtypedetail.this);
//            @Override
//
//            public Map<String, String> getHeaders()  {
//                Map<String, String> params = new HashMap<String, String>();
//                params.put("Content-Type", "application/json");
//                params.put("Authorization","bare "+sessionclass.getToken());
//                return params;
//            }
//        };
//        requestQueue.add(objectRequest);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selectedFromList = (listView.getItemAtPosition(position).toString());

                Intent intent = new Intent(listView.getContext(),chat.class);
                intent.putExtra("email", email);
                intent.putExtra("sendemail", selectedFromList);


                listView.getContext().startActivity(intent);
            }
        });
    }



    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menubloodtypedetail, menu);
        return super.onCreateOptionsMenu(menu);
    }
    public void back(MenuItem item){
        Intent myIntent = new Intent(this, showuserdetail.class);

        this.startActivity(myIntent);
    }


}
