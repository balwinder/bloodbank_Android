package com.example.balwindersingh.bloodhelp;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.arturogutierrez.Badges;
import com.github.arturogutierrez.BadgesNotSupportedException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;


public class chat extends AppCompatActivity {
    ipaddress ipadress = new ipaddress();
    String email, sendemail;
    io.socket.client.Socket socket;
    ListView mListView;
    ArrayAdapter adapter;
    TextView emailtext;
    ArrayList<String> mobileArray = new ArrayList<>();
    private static final int REQUEST_CODE = 1;
    private static final int NOTIFICATION_ID = 6578;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.chatroom);
        emailtext = findViewById(R.id.emailtext);
        mListView = findViewById(R.id.list);
        adapter = new ArrayAdapter<>(this, R.layout.activity_listview, mobileArray);
        mListView.setAdapter(adapter);

        try {
            socket = IO.socket(ipadress.ip);
            Toast.makeText(getApplicationContext(), "connected", Toast.LENGTH_LONG).show();


        } catch (URISyntaxException e) {
            Log.d("Hz", e.getMessage());
        }

        Intent intent = getIntent();
        //email = intent.getStringExtra("email");
        sendemail = intent.getStringExtra("sendemail");
        emailtext.setText(sendemail);
        Toast.makeText(getApplicationContext(), "this " + email, Toast.LENGTH_LONG).show();
        //mobileArray.clear();
        Connect(null);


    }

    public void sendMessage(View view) {
        EditText editText = findViewById(R.id.editText);
        //EditText editText1 =  findViewById(R.id.editText2);
        JSONObject obj = new JSONObject();
        sessionclass sessionclass = new sessionclass(chat.this);
        try {

            obj.put("Message", "/w " + sendemail + " " + editText.getText().toString());
            obj.put("sendEmail", sessionclass.getName());
            obj.put("reciveEmail", emailtext.getText());
            //  obj.put("User", editText1.getText().toString());
            // obj.put("sendto","bhatti");
        } catch (JSONException e) {
            Log.d("Hz", e.getMessage());
        }
        mobileArray.add("You : " + editText.getText().toString());
        adapter.notifyDataSetChanged();
        mListView.setSelection(adapter.getCount() - 1);
        editText.setText("");
        socket.emit("send message", obj);


    }

    public void Connect(View view) {

        socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {


            @Override
            public void call(Object... args) {

                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(getApplicationContext(), "I think Server Connected", Toast.LENGTH_LONG).show();
                    }
                });
                JSONObject obj = new JSONObject();
                try {
                    sessionclass sessionclass = new sessionclass(chat.this);
                    obj.put("User", sessionclass.getName());
                } catch (JSONException e) {
                    Log.d("Hz", e.getMessage());
                }
                socket.emit("UserConnected", obj);
            }

        }).on("NewMsg", new Emitter.Listener() {

            @Override
            public void call(final Object... args) {

                final JSONObject data = (JSONObject) args[0];

                runOnUiThread(new Runnable() {
                    public void run() {

                        try {
                            String Msg = data.getString("Message");


                            String emailfromserver = data.getString("Email");
                            Toast.makeText(getApplicationContext(), "hey " + emailfromserver, Toast.LENGTH_LONG).show();
                            if (emailfromserver.equals(emailtext.getText())) {
                                // Toast.makeText(getApplicationContext(), "hello",Toast.LENGTH_LONG).show()

                                mobileArray.add(Msg);
                                adapter.notifyDataSetChanged();
                                mListView.setSelection(adapter.getCount() - 1);
                            } else {
                                Toast.makeText(getApplicationContext(), "invalid email", Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            Log.d("Hz", e.getMessage());
                        }
                    }
                });
            }

        }).on("load", new Emitter.Listener() {

            @Override
            public void call(final Object... args) {

                final JSONObject data = (JSONObject) args[0];
                runOnUiThread(new Runnable() {
                    public void run() {
                        try {
                            String Msg = data.getString("Mesasge");
                            String Count = data.getString("Count");
                            mobileArray.clear();
                            int count = Integer.parseInt(Count);
                            JSONArray jsonArr = new JSONArray(Msg);
                            for (int i = 0; i < count; i++) {
                                String list = jsonArr.getJSONObject(i).getString("message");
                                String sendemail = jsonArr.getJSONObject(i).getString("sendemail");

                                String reciveEmail = jsonArr.getJSONObject(i).getString("reciveEmail");
                                Toast.makeText(getApplicationContext(), "login:      " + sendemail + "jinu send karna " + reciveEmail, Toast.LENGTH_LONG).show();
                                // Toast.makeText(getApplicationContext(), "old:      " + Emailfromserver, Toast.LENGTH_LONG).show();
                                sessionclass sessionclass = new sessionclass(getApplicationContext());
                                if ((sessionclass.getName().equals(sendemail) && emailtext.getText().equals(reciveEmail)) || (sessionclass.getName().equals(reciveEmail) && emailtext.getText().equals(sendemail))) {
                                    // mobileArray.clear();
                                    if ((sessionclass.getName().equals(sendemail))) {
                                        mobileArray.add("you : " + list);
                                        adapter.notifyDataSetChanged();
                                        mListView.setSelection(adapter.getCount() - 1);
                                    } else {
                                        //
                                        mobileArray.add(list);

//                                    ListAdapter listAdapter = new ArrayAdapter<>(getApplication(), android.R.layout.simple_list_item_1, mobileArray);
//                                    mListView.setAdapter(listAdapter);
                                        adapter.notifyDataSetChanged();
                                        mListView.setSelection(adapter.getCount() - 1);
                                    }


                                } else {
                                    Toast.makeText(getApplicationContext(), "diffrent chat", Toast.LENGTH_LONG).show();
                                }
                            }
                        } catch (JSONException e) {
                            Log.d("Hz", e.getMessage());
                        }
                    }
                });
            }
        });
        socket.connect();
    }

    //Notification

    private void showNotifications(String title, String msg) {
        sessionclass sessionclass = new sessionclass(chat.this);
        Intent i = new Intent(this, chat.class);


        PendingIntent pendingIntent = PendingIntent.getActivity(this, REQUEST_CODE,
                i, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notification = new NotificationCompat.Builder(this)
                .setContentText(msg)
                .setContentTitle(title)
                .setContentIntent(pendingIntent)
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .build();

        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        manager.notify(NOTIFICATION_ID, notification);


    }


}

