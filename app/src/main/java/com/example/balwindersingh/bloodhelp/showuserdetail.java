package com.example.balwindersingh.bloodhelp;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class showuserdetail extends AppCompatActivity implements View.OnClickListener {

    ipaddress ipadress = new ipaddress();

    String result;
    String latitude;
    String longitude;
    String tokennumber;
    String email;
    Button logout,onagative,opositive,apositive,anagative,abpositive,abnagative,bpositive,bnagative;
    @SuppressLint("MissingPermission")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.showuserdetail);
      logout =findViewById(R.id.logout);
      onagative = findViewById(R.id.onagative);
        onagative.setOnClickListener(this);
        opositive = findViewById(R.id.opositive );
        opositive.setOnClickListener(this);
                apositive = findViewById(R.id.apositive);
        apositive.setOnClickListener(this);
                 anagative = findViewById(R.id.Anagative);
        anagative.setOnClickListener(this);
                bnagative = findViewById(R.id.bnagative);
        bnagative.setOnClickListener(this);
               bpositive = findViewById(R.id.bpositive);
        bpositive.setOnClickListener(this);
              abpositive = findViewById(R.id.abpositive);
        abpositive.setOnClickListener(this);
                   abnagative = findViewById(R.id.abnagative);
        abnagative.setOnClickListener(this);
        Intent intent = getIntent();
        tokennumber = intent.getStringExtra("token");
         email = intent.getStringExtra("email");


        logout();
        //Onagative();
        LocationManager mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10,10, mLocationListener);

        List<String> providers = mLocationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            Location l = mLocationManager.getLastKnownLocation(provider);


            if (l == null) {
                continue;
            }
            if (bestLocation == null
                    || l.getAccuracy() < bestLocation.getAccuracy()) {

                bestLocation = l;
                latitude=String.valueOf(bestLocation.getLatitude());
                longitude=String.valueOf(bestLocation.getLongitude());

            }
        }
        if (bestLocation == null) {
           // return null;
        }
       // return bestLocation;

    }


    private final LocationListener mLocationListener = new LocationListener() {

        @Override
        public void onLocationChanged(final Location location) {
           // latitude=String.valueOf(location.getLatitude());
     // longitude=String.valueOf(location.getLongitude());
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };





    public void logout(){
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new sessionclass(showuserdetail.this).removeuser();
                Intent intent=new Intent(showuserdetail.this,MainActivity.class);
                sessionclass sessionclass = new sessionclass(getApplicationContext());
                sessionclass.setName("");
                startActivity(intent);
                finish();
            }
        });
    }


    @Override
    public void onClick(View v) {


        switch (v.getId()) {

            case R.id.onagative:

                callserver("O-");
                break;

            case R.id.opositive:
               callserver("O+");

                break;
            case R.id.Anagative:

                callserver("A-");
                break;
            case R.id.apositive:
                callserver("A+");

                break;
            case R.id.bpositive:
               callserver("B+");

                break;
            case R.id.bnagative:
                callserver("B-");

                break;

            case R.id.abnagative:
                callserver("AB-");
                // do your code
                break;
            case R.id.abpositive:
              callserver("AB+");
                //do your code
                break;

            default:
                break;
        }


        }
    @SuppressLint("MissingPermission")
    public void callserver(String bloodtype){

        final RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        //+bloodtype


        Toast.makeText(getApplicationContext(),longitude ,Toast.LENGTH_SHORT).show();
        String JsonURL =   ipadress.ip+"/user/"+bloodtype+"/?lat="+latitude+"&lng="+longitude;
       // String JsonURL = "http://192.168.1.54:3000/user/"+bloodgroup;
       HashMap data = new HashMap();
//        Toast.makeText(getApplicationContext(),longitude + " ",Toast.LENGTH_LONG).show();
      data.put("longitude",longitude);
      data.put("latitude",latitude);
        JsonObjectRequest objectRequest = new JsonObjectRequest(Request.Method.GET, JsonURL,new Response.Listener<JSONObject>() {
            public void onResponse(JSONObject response) {
                try {
                    //  JSONObject name = response.getJSONObject("info");
                    JSONArray name2 = response.getJSONArray("User2");

                    result = response.getString("count");

                    Intent intent=new Intent(showuserdetail.this,bloodtypedetail.class);
                    intent.putExtra("count",result);
                    intent.putExtra("User2", String.valueOf(name2));
                    intent.putExtra("email", email);
                    startActivity(intent);
                    finish();


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }
        ) {

            /**
             * Passing some request headers*
             */
            final sessionclass sessionclass = new sessionclass(showuserdetail.this);
            @Override

            public Map<String, String> getHeaders()  {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization","bare "+sessionclass.getToken());
                return params;
            }
        };
        requestQueue.add(objectRequest);

    }


}
