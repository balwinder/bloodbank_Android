package com.example.balwindersingh.bloodhelp;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class signup extends AppCompatActivity{
    ipaddress ipadress = new ipaddress();
    //Harpal Singh Pannu
    Button createAccount;
    EditText email,password,name,phonnumber;
    RadioGroup gendergroup;
    Spinner spiner;
    RadioButton radioButtons;
    String text;
    String bloodgroup;
    String name1;
    String latitude;
    String longitude;
    String[] mystringarray ;
   // Cursor count;
    @SuppressLint("MissingPermission")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup);
        createAccount = findViewById(R.id.createaccount);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        name = findViewById(R.id.name);
        phonnumber = findViewById(R.id.phonnumber);
        gendergroup = findViewById(R.id.gendergroup);


//map cordinates
        LocationManager mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2000,10, mLocationListener);



        ArrayAdapter<String> myadapter = new ArrayAdapter<>(signup.this,
                android.R.layout.simple_list_item_1,getResources().getStringArray(R.array.names));
        spiner = findViewById(R.id.spiner);
        myadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spiner.setAdapter(myadapter);




        createAccount();
    }
//getting log and latti
private final LocationListener mLocationListener = new LocationListener() {
    @Override
    public void onLocationChanged(final Location location) {

        latitude = String.valueOf(location.getLatitude());
        longitude = String.valueOf(location.getLongitude());
      mystringarray =new String[]{longitude,latitude};
       // Toast.makeText(getApplicationContext(),longitude + " ",Toast.LENGTH_LONG).show();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
};

    public static boolean isValidEmailAddress(String emailAddress) {
        String emailRegEx;
        Pattern pattern;
        // Regex for a valid email address
        emailRegEx = "^[A-Za-z0-9._%+\\-]+@[A-Za-z0-9.\\-]+\\.[A-Za-z]{2,4}$";
        // Compare the regex with the email address
        pattern = Pattern.compile(emailRegEx);
        Matcher matcher = pattern.matcher(emailAddress);
        if (!matcher.find()) {
            return false;
        }
        return true;
    }




    public void radiogroup(View v){
        int radiobuttonid = gendergroup.getCheckedRadioButtonId();
        radioButtons =  findViewById(radiobuttonid);
        text = (String) radioButtons.getText();

        Toast.makeText(getBaseContext(),radioButtons.getText(),Toast.LENGTH_LONG).show();
    }

    public void createAccount(){
        createAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //front checking all right
                if (name.getText().length() == 0) {
                    Toast.makeText(signup.this, "enter the name", Toast.LENGTH_SHORT).show();
                    name.requestFocus();
                } else if (email.getText().length() == 0) {
                    Toast.makeText(signup.this, "enter the email", Toast.LENGTH_SHORT).show();
                    email.requestFocus();
                }
//                        else if(count.getCount()>=1){
//                            Toast.makeText(signup.this,"this email addres is already usaed ",Toast.LENGTH_LONG).show();
//                            userid1.requestFocus();
//                        }
                else if (password.getText().length() == 0) {
                    Toast.makeText(signup.this, "enter the password", Toast.LENGTH_LONG).show();
                    password.requestFocus();

                } else if (phonnumber.getText().length() < 10 || phonnumber.getText().length() > 10) {

                    Toast.makeText(signup.this, "enter valid phone number", Toast.LENGTH_LONG).show();
                    phonnumber.requestFocus();
                    phonnumber.setError("entr valid phon number");
                } else if (text == null) {

                    Toast.makeText(signup.this, " select Gender ", Toast.LENGTH_LONG).show();

                } else if (spiner.getSelectedItem().toString().equals("choose")) {
                    Toast.makeText(signup.this, "choose a blood group ", Toast.LENGTH_LONG).show();
                    spiner.requestFocus();
                } else if (isValidEmailAddress(email.getText().toString()) == false) {
                    Toast.makeText(signup.this, "enter valid email id  ", Toast.LENGTH_LONG).show();
                    email.requestFocus();
                } else {


                    bloodgroup = spiner.getSelectedItem().toString();
                    //send request to server for signup
                    final RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

                    String JsonURL = ipadress.ip+"/user/signup";



                        HashMap data = new HashMap();
                    data.put("email", email.getText().toString());
                    data.put("password", password.getText().toString());
                    data.put("name", name.getText().toString());
                    data.put("phonenumber", password.getText().toString());
                    data.put("gender", text);
                    data.put("bloodgroup", bloodgroup);
                  data.put("Coordinates",mystringarray);

                    //data.put("phonnumber")

                    JsonObjectRequest objectRequest = new JsonObjectRequest(Request.Method.POST, JsonURL, new JSONObject(data), new Response.Listener<JSONObject>() {

                        public void onResponse(JSONObject response) {
                            Log.d("Hz", response.toString());


                            try {
                                 name1 = response.get("message").toString();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            if (name1.equals("Mailexists")) {
                                Toast.makeText(getApplicationContext(), name1, Toast.LENGTH_LONG).show();
                                email.setError("email already exist");
                                Toast.makeText(getApplicationContext(), "email already exist", Toast.LENGTH_LONG).show();
                            } else {
                                name.setText("");
                                phonnumber.setText("");
                                email.setText("");
                                password.setText("");
                                gendergroup.clearCheck();
                                //radioButtons.toggle();
                                spiner.setSelection(0);

                                Intent intent = new Intent(signup.this, MainActivity.class);
                                startActivity(intent);

                                Toast.makeText(getApplicationContext(), response.toString(), Toast.LENGTH_LONG).show();
//                           // JSONArray name2 = response.getJSONArray("info");
//                            JSONArray name1 = response.getJSONArray("results");
//                            //Toast.makeText(getApplicationContext(),name2.getJSONArray(0).toString(),Toast.LENGTH_LONG).show();
//                           // Toast.makeText(getApplicationContext(),name.getString("seed"),Toast.LENGTH_LONG).show();
//                            //Toast.makeText(getApplicationContext(),name1.getJSONObject(0).getJSONObject("name").getString("first"),Toast.LENGTH_LONG).show();
//                            //Toast.makeText(getApplicationContext(),name.getString("page"),Toast.LENGTH_LONG).show();
//                           // Toast.makeText(getApplicationContext(),name1.getJSONObject(0).getString("gender"),Toast.LENGTH_LONG).show();
//                            Toast.makeText(getApplicationContext(),name1.getJSONObject(0).getJSONObject("name").getString("first"),Toast.LENGTH_LONG).show();
                                // Toast.makeText(getApplicationContext(),name2.getString(0).toString(),Toast.LENGTH_LONG).show();


                            }
                        }

                    }, new Response.ErrorListener() {
                        public void onErrorResponse(VolleyError error) {
                            error.printStackTrace();
                        }
                    }
                    );
                    requestQueue.add(objectRequest);


                }
            }

        });



            }


}
