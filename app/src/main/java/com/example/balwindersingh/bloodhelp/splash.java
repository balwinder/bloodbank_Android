package com.example.balwindersingh.bloodhelp;

import android.content.Intent;
import android.os.Bundle;
//import android.support.design.widget.FloatingActionButton;
//import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class splash extends AppCompatActivity {
    ipaddress ipadress = new ipaddress();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                final sessionclass sessionclass = new sessionclass(splash.this);
                if(sessionclass.getToken()!="") {


                    final RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

                    String JsonURL = ipadress.ip+"/user/";
                    JsonObjectRequest objectRequest = new JsonObjectRequest(Request.Method.POST, JsonURL,new Response.Listener<JSONObject>() {

                        public void onResponse(JSONObject response) {
                        Log.d("Hz",response.toString());
                       try {
                           String name = response.toString();


                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                           // Toast.makeText(getApplicationContext(),sessionclass.getToken(),Toast.LENGTH_LONG).show();

                        }

                    }, new Response.ErrorListener() {
                        public void onErrorResponse(VolleyError error) {
                            error.printStackTrace();
                        }
                    }
                    ) {

                        /**
                         * Passing some request headers*
                         */
                        @Override
                        public Map<String, String> getHeaders()  {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("Content-Type", "application/json");
                            params.put("Authorization","bare "+sessionclass.getToken());
                            return params;
                        }
                    };
                    requestQueue.add(objectRequest);

                     Intent intent = new Intent(splash.this, showuserdetail.class);

                   // intent.putExtra("Name",sessionclass.getName());
                   // intent.putExtra("password",sessionclass.getPass());
                    // Toast.makeText(getApplicationContext(),sessionclass.getName(),S)
                    //intent.putExtra("pass",sessionclass.getPass());

                    startActivity(intent);
                   // finish();
                }else {

                    startActivity(new Intent(splash.this,MainActivity.class));
                    finish();
                }
            }
        },1000);
    }
}
